Inspired by [jenv](https://github.com/gcuisinier/jenv) this simple shell script selects the correct version of the [Glasgow Haskell Compiler](https://www.haskell.org/ghc/) (`ghc`, `ghci`, etc) on a per-project basis via a `.ghc-version` file.

To install, clone this repo and add the `bin` directory to the front of your `PATH`.

```
# one time, upgrade when you like
git clone https://gitlab.com/fommil/ghc-env.git ~/.ghc-env

# into ~/.profile
export PATH=$HOME/.ghc-env/bin:$PATH
```

It hijacks the following binaries and delegates:

```
ghc  ghci  ghc-pkg  haddock  hp2ps  hpc  hsc2hs  runghc  runhaskell
```

## `compile`

To compile any version of ghc from source:

```
ghc-env compile 8.4.3 8.4.2
```

The first version is the one to download and build.

The second version is the (installed) `ghc` used to bootstrap the compile. If this is omitted the `default` version is used.

This can take anywhere between 20 minutes and several hours, depending on the performance of your computer.

Make sure you've prepared your system accordingly https://ghc.haskell.org/trac/ghc/wiki/Building/Preparation/Linux

## `install`

To install pre-compiled binaries from https://www.haskell.org simply select the version to install

```
ghc-env install 8.4.3
```

The logic to detect the file to download is very fragile, if it fails just manually download the version you wish to install from https://downloads.haskell.org/~ghc into `ghc-env/lib/ghc-X.Y.Z.tar.xz` and run the command again.

## `add`

To add a ghc installation use

```
ghc-env add /path/to/installation
```

## `.ghc-version`

A typical `.ghc-version` file contains the version of `ghc` to use

```
8.4.3
```

## Cabal

You no longer need global cabal installations, use the `~/.cabal/config` setting `require-sandbox: True` to avoid accidents.
